<?php

require_once $GLOBALS['ROOT_PATH'].'\\App\\Http\\Middleware\\AuthMiddleware.php';

class Middleware
{
    const ARRAY = ['auth'=>AuthMiddleware::class];
}