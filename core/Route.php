<?php
require_once $GLOBALS['ROOT_PATH'].'\\App\\Http\\Controllers\\AuthController.php';
require_once $GLOBALS['ROOT_PATH'].'\\Core\\Middleware.php';

class Route
{
    private static array $routes=[];
    public static function get($path,$callback)
    {
        self::$routes['GET'][$path]=[
            'callback'=>$callback,
            'middleware'=>null
        ];
        return self::class;
    }

    public static function only($key)
    {
        $index = array_key_last(self::$routes);
        $path = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];
        self::$routes[$method][$path]['middleware'] = $key; 
    }

    public static function resolve()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $path = $_SERVER['REQUEST_URI'];
        $routes = self::$routes;
        
        if(!key_exists($method,$routes)) die('Method not supported');
        
        if(!key_exists($path,$routes[$method])) die('Route not found');

        if($routes[$method][$path]['middleware']!==null)
        {
            $class = Middleware::ARRAY['auth'];
            (new $class)->handle();
        }

        $callback = $routes[$method][$path]['callback'];

        if(gettype($callback)=='string')
        {
            echo $callback;
        }

        if(gettype($callback)=='object')
        {
            $callback();
        }

        if(gettype($callback)=='array')
        {
            $callback[0] = new $callback[0]();
            call_user_func($callback);
        }
        
        

    }
}